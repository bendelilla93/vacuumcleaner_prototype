﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Detector;

namespace Cleaner
{
    public class Cleaner : ICleaner
    {
        public void StartCleaning()
        {
            Console.WriteLine("The cleaning has started.");
            bool isNewLine = false;

            do
            {
                Go("ahead");
                StandStill();
                LineFinished(false);
                Go("back");
                StandStill();
                LineFinished(true);
                isNewLine = ChangeLinesPossible();
                ChangeLines(isNewLine);
            } while (isNewLine);

            Console.WriteLine("The cleaning has finished.");
        }

        private void Go(string Direction)
        {
            IDetector detector = new Detector.Detector();
            Console.WriteLine("The robot has started going.");
            detector.Watch(Direction);
        }

        private void StandStill()
        {
            Console.WriteLine("The robot is standing still.");
        }

        private bool LineFinished(bool finished)
        {
            if (finished)
            {
                Console.WriteLine("The robot has to change lines."); 
                return true;
            }
            else
            {
                Console.WriteLine("The robot has to finish the line.");
                return false;
            }
        }

        private bool ChangeLinesPossible()
        {
            Console.WriteLine("The robot tries to change lines");
            IDetector detector = new Detector.Detector();
            return !detector.Obstacle("left");
        }

        private void ChangeLines(bool isNewLine)
        {
            if (isNewLine)
            {
                Console.WriteLine("The robot has gone to the next line.");
            }
            else
            {
                Console.WriteLine("There is no more line.");
            }
        }
    }

}
