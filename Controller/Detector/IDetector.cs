﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Detector
{
    public interface IDetector
    {
        bool Watch(string Direction);
        bool Obstacle(string Direction);
    }
}
