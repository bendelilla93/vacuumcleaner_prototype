﻿using System;

namespace Detector
{
    internal class Sensor
    {
        private string direction;
        public Sensor(string dir)
        {
            direction = dir;
            Console.WriteLine("Sensor has been created for direction: " + direction);
        }

        public bool SthDetected()
        {
            System.Random x = new System.Random();
            if(x.NextDouble() > 0.5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}