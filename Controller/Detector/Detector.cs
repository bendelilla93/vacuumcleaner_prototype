﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Detector
{
    public class Detector : IDetector
    {
        public bool Obstacle(string Direction)
        {
            Sensor x = new Sensor(Direction);
            return x.SthDetected();
        }

        public bool Watch(string Direction)
        {
            Sensor x = new Sensor(Direction);
            bool obstacle = false;
            while (!obstacle)
            {
                System.Threading.Thread.Sleep(1000);
                Console.WriteLine("Watch: 1 sec has passed");
                obstacle = x.SthDetected();
            }
            return true;
        }
    }
}
