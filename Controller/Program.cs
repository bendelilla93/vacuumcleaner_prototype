﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cleaner;

namespace Controller
{
    class Program
    {
        static void Main(string[] args)
        {
            ICleaner vacuumCleaner = new Cleaner.Cleaner();
            vacuumCleaner.StartCleaning();
            Console.ReadLine();
        }
    }
}
